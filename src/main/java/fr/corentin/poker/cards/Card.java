package fr.corentin.poker.cards;

import fr.corentin.poker.cards.enums.CardType;
import fr.corentin.poker.cards.enums.CardColor;
import fr.corentin.poker.cards.enums.CardSymbol;

public class Card implements Comparable<Card>{

    private final int order;

    private final CardSymbol cardSymbol;

    private final CardColor cardColor;

    private final CardType cardType;

    public Card(String cardType, CardSymbol cardSymbol) {
        this.cardType = CardType.getValue(cardType);
        this.order = this.cardType.getOrder();
        this.cardSymbol = cardSymbol;
        this.cardColor = this.cardSymbol.getColor();
    }

    public Card(int order, CardSymbol cardSymbol) {
        this.cardType = CardType.getIdByValue(order);
        this.order = order;
        this.cardSymbol = cardSymbol;
        this.cardColor = this.cardSymbol.getColor();
    }

    public int getOrder() {
        return order;
    }

    public CardSymbol getSymbol() {
        return cardSymbol;
    }

    public CardColor getColor() {
        return cardColor;
    }

    public CardType getCardType() {
        return cardType;
    }

    @Override
    public String toString() {
        return this.cardType.getFormattedCardValue() + this.cardSymbol.getFormattedSymbol();
    }

    @Override
    public int compareTo(Card c) {
        return Integer.compare(this.getOrder(), c.getOrder());
    }
}
