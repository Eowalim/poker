package fr.corentin.poker.cards;

import fr.corentin.poker.cards.enums.CardSymbol;
import fr.corentin.poker.game.Poker;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Deck {
    private final LinkedList<Card> deck;
    private final LinkedList<Card> playedCards;

    public Deck() {
        this.deck = new LinkedList<>();
        this.playedCards = new LinkedList<>();
        this.createDeck();
    }

    private void createDeck() {
        if (!this.deck.isEmpty()) this.deck.clear();
        CardSymbol[] cardSymbols = CardSymbol.values();
        for (int i = 0; i < 4; i++) {
            for (int j = 1; j <= 13; j++) {
                this.deck.add(new Card(j, cardSymbols[i]));
            }
        }
        this.check();
    }

    public void mix() {
        Collections.shuffle(this.deck);
    }

    public void reset() {
        this.createDeck();
    }

    public Card drawCard() {
        if (Poker.DISCARD_CARD) {
           undoCard();
        }
        final Card give = this.deck.pop();
        this.playedCards.add(give);
        return give;
    }

    public void undoCard(){
        Card undo = this.deck.pop();
        this.playedCards.add(undo);
    }

    public void check() {
        if (this.deck.size() == 52) return;
        throw new IllegalStateException("The deck doesn't contain all 52 cards.");
    }

    public List<Card> getDeck() {
        return deck;
    }

    public List<Card> getPlayedCards() {
        return playedCards;
    }
}
