package fr.corentin.poker.cards.enums;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum CardType {
    _2(1, "2"),
    _3(2, "3"),
    _4(3, "4"),
    _5(4, "5"),
    _6(5, "6"),
    _7(6, "7"),
    _8(7, "8"),
    _9(8, "9"),
    _10(9, "10"),
    VALET(10, "J"),
    DAME(11, "Q"),
    ROI(12, "K"),
    AS(13, "A");


    private final int order;

    private final String formattedCardValue;

    CardType(int order, String formattedCardValue) {
        this.order = order;
        this.formattedCardValue = formattedCardValue;
    }

    public int getOrder() {
        return order;
    }

    public String getFormattedCardValue() {
        return formattedCardValue;
    }

    public static CardType getIdByValue(int id) {
        if (id < 1 || id > 13) {
            throw new IllegalArgumentException("This id : " + id + " doesn't exists !");
        }
        return Stream.of(CardType.values()).filter(v -> v.getOrder() == id).findFirst().get();
    }

    public static CardType getValue(String value) {
        Set<String> values = Stream.of(CardType.values()).map(CardType::getFormattedCardValue).collect(Collectors.toSet());
        if (!values.contains(value)) {
            throw new IllegalArgumentException("This card value : " + value + " doesn't exists !");
        }
        return Stream.of(CardType.values()).filter(v -> v.getFormattedCardValue().equalsIgnoreCase(value)).findFirst().get();
    }
}
