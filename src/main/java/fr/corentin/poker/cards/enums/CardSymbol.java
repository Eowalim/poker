package fr.corentin.poker.cards.enums;

public enum CardSymbol {
    DIAMOND ("♦", CardColor.RED),
    SPADES ("♠", CardColor.BLACK),
    HEART ("♥", CardColor.RED),
    CLUB ("♣", CardColor.BLACK);

    private final String formattedSymbol;
    private final CardColor cardColor;

    CardSymbol(String formattedSymbol, CardColor cardColor) {
        this.formattedSymbol = formattedSymbol;
        this.cardColor = cardColor;
    }

    public String getFormattedSymbol() {
        return formattedSymbol;
    }

    public CardColor getColor() {
        return cardColor;
    }
}
