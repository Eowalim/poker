package fr.corentin.poker.cards.enums;

public enum CardColor {
    RED,
    BLACK;
}
