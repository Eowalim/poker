package fr.corentin.poker;

import fr.corentin.poker.game.HandCombination;
import fr.corentin.poker.player.Player;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class HandCombinationTest {
    @Test
    void testHighCardCombination() {
        test(HandCombination.HIGH_CARD);
    }

    @Test
    void testPairCombination() {
        test(HandCombination.PAIR);
    }

    @Test
    void testDoublePairCombination() {
        test(HandCombination.DOUBLE_PAIR);
    }

    @Test
    void testThreeOfKingCombination() {
        test(HandCombination.THREE_OF_KIND);
    }

    @Test
    void testQuinteCombination() {
        test(HandCombination.QUINTE);
    }

    @Test
    void testFullCombination() {
        test(HandCombination.FULL);
    }

    @Test
    void testFlushCombination() {
        test(HandCombination.FLUSH);
    }

    @Test
    void testFourOfKindCombination() {
        test(HandCombination.FOUR_OF_KIND);
    }

    @Test
    void testQuinteFlushCombination() {
        test(HandCombination.QUINTE_FLUSH);
    }

    @Test
    void testQuinteFlushRoyaleCombination() {
        test(HandCombination.QUINTE_FLUSH_ROYALE);
    }


    void test(HandCombination combination) {
        Player player = new Player();
        player.getHand().addCardsToHand(combination.getExampleHand());
        player.analyzeHand();
        Assertions.assertEquals(combination, player.getCombination(), "Test failed on: " + combination);
        System.out.println(combination + ": Passed");
    }
}
