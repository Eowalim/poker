package fr.corentin.poker;

import fr.corentin.poker.game.Poker;
import fr.corentin.poker.player.Player;

public class Main {

    public static int NB_PLAYER = 4;

    public static void main(String[] args) {
        /**
         * 52 cards in deck
         * 5 cards for table and 2 cards in hand
         * DISCARD_CARD(true) => ((52 - 5) / 2) / 2 = 11.17 thus 11 players max
         * DISCARD_CARD(false) => ((52 - 5) / 2 = 23.5 thus 23 players max
         */
        if (Poker.DISCARD_CARD && NB_PLAYER > 11){
            throw new IllegalStateException("The maximum number of players is 11 if you discard a card each time.");
        }

        if (!Poker.DISCARD_CARD && NB_PLAYER > 23){
            throw new IllegalStateException("The maximum number of players is 23 if you do not discard a card each time.");
        }

        Poker poker = new Poker();
        poker.init();

        for (int i = 0; i < NB_PLAYER; i++) {
            poker.addPlayer(new Player());
        }

        poker.giveHands();
        poker.showAllHands();

        System.out.println();

        System.out.println("-> Table (tour 1) : ");
        poker.addFirstThreeCardsOnTable();
        poker.showCardsOnTable();

        for (int i = 1; i<= 2; i++){
            System.out.println("-> Table (tour " + (i + 1) + ") : ");
            poker.addCardsOnTable();
            poker.showCardsOnTable();
        }

        System.out.println();
        poker.printResult();


    }
}