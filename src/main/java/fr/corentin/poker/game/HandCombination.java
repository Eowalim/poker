package fr.corentin.poker.game;

import fr.corentin.poker.cards.Card;
import fr.corentin.poker.cards.enums.CardSymbol;

import java.util.ArrayList;
import java.util.List;

public enum HandCombination {
    HIGH_CARD(10, new ArrayList<>() {{
        add(new Card("K", CardSymbol.HEART));
        add(new Card("J", CardSymbol.DIAMOND));
        add(new Card("9", CardSymbol.SPADES));
        add(new Card("6", CardSymbol.CLUB));
        add(new Card("3", CardSymbol.DIAMOND));
        add(new Card("2", CardSymbol.HEART));
        add(new Card("Q", CardSymbol.SPADES));
    }}),

    PAIR(20, new ArrayList<>() {{
        add(new Card("2", CardSymbol.CLUB));
        add(new Card("2", CardSymbol.DIAMOND));
        add(new Card("K", CardSymbol.HEART));
        add(new Card("10", CardSymbol.SPADES));
        add(new Card("9", CardSymbol.DIAMOND));
        add(new Card("3", CardSymbol.CLUB));
        add(new Card("4", CardSymbol.SPADES));
    }}),

    DOUBLE_PAIR(30, new ArrayList<>() {{
        add(new Card("A", CardSymbol.HEART));
        add(new Card("A", CardSymbol.DIAMOND));
        add(new Card("5", CardSymbol.SPADES));
        add(new Card("5", CardSymbol.CLUB));
        add(new Card("8", CardSymbol.HEART));
        add(new Card("3", CardSymbol.CLUB));
        add(new Card("4", CardSymbol.SPADES));
    }}),

    THREE_OF_KIND(40, new ArrayList<>() {{
        add(new Card("J", CardSymbol.CLUB));
        add(new Card("J", CardSymbol.DIAMOND));
        add(new Card("J", CardSymbol.SPADES));
        add(new Card("8", CardSymbol.DIAMOND));
        add(new Card("6", CardSymbol.CLUB));
        add(new Card("3", CardSymbol.HEART));
        add(new Card("4", CardSymbol.SPADES));
    }}),

    QUINTE(50, new ArrayList<>() {{
        add(new Card("5", CardSymbol.HEART));
        add(new Card("6", CardSymbol.DIAMOND));
        add(new Card("7", CardSymbol.SPADES));
        add(new Card("8", CardSymbol.SPADES));
        add(new Card("9", CardSymbol.CLUB));
        add(new Card("3", CardSymbol.HEART));
        add(new Card("4", CardSymbol.HEART));
    }}),

    FLUSH(60, new ArrayList<>() {{
        add(new Card("2", CardSymbol.SPADES));
        add(new Card("3", CardSymbol.SPADES));
        add(new Card("9", CardSymbol.SPADES));
        add(new Card("10", CardSymbol.SPADES));
        add(new Card("J", CardSymbol.SPADES));
        add(new Card("5", CardSymbol.HEART));
        add(new Card("7", CardSymbol.SPADES));
    }}),

    FULL(70, new ArrayList<>() {{
        add(new Card("3", CardSymbol.CLUB));
        add(new Card("3", CardSymbol.DIAMOND));
        add(new Card("3", CardSymbol.HEART));
        add(new Card("2", CardSymbol.SPADES));
        add(new Card("2", CardSymbol.DIAMOND));
        add(new Card("5", CardSymbol.HEART));
        add(new Card("7", CardSymbol.SPADES));
    }}),

    FOUR_OF_KIND(80, new ArrayList<>() {{
        add(new Card("A", CardSymbol.HEART));
        add(new Card("A", CardSymbol.DIAMOND));
        add(new Card("A", CardSymbol.SPADES));
        add(new Card("A", CardSymbol.CLUB));
        add(new Card("10", CardSymbol.DIAMOND));
        add(new Card("5", CardSymbol.HEART));
        add(new Card("7", CardSymbol.SPADES));
    }}),

    QUINTE_FLUSH(90, new ArrayList<>() {{
        add(new Card("9", CardSymbol.DIAMOND));
        add(new Card("10", CardSymbol.DIAMOND));
        add(new Card("J", CardSymbol.DIAMOND));
        add(new Card("Q", CardSymbol.DIAMOND));
        add(new Card("K", CardSymbol.DIAMOND));
        add(new Card("5", CardSymbol.CLUB));
        add(new Card("7", CardSymbol.SPADES));
    }}),

    QUINTE_FLUSH_ROYALE(100, new ArrayList<>() {{
        add(new Card("10", CardSymbol.HEART));
        add(new Card("J", CardSymbol.HEART));
        add(new Card("Q", CardSymbol.HEART));
        add(new Card("K", CardSymbol.HEART));
        add(new Card("A", CardSymbol.HEART));
        add(new Card("5", CardSymbol.CLUB));
        add(new Card("7", CardSymbol.SPADES));
    }});


    private final int points;
    private final List<Card> exampleHand;


    HandCombination(int points, List<Card> exampleHand) {
        this.points = points;
        this.exampleHand = exampleHand;
    }

    public int getPoints() {
        return points;
    }

    public List<Card> getExampleHand() {
        return exampleHand;
    }
}
