package fr.corentin.poker.game;

import fr.corentin.poker.cards.Deck;
import fr.corentin.poker.player.Player;
import fr.corentin.poker.player.Table;

import java.util.ArrayList;
import java.util.List;

public class Poker {
    public static boolean DISCARD_CARD = true;

    private final List<Player> players;

    private final Deck deck;
    private final Table table;


    public Poker() {
        this.players = new ArrayList<>();
        this.deck = new Deck();
        this.table = new Table();
    }

    public void init(){
        deck.mix();
    }
    public void addFirstThreeCardsOnTable() {
        for (int i = 0; i < 3; i++) {
            this.table.addCardOnTable(deck.drawCard());
        }
    }

    public void addCardsOnTable(){
        this.table.addCardOnTable(deck.drawCard());
    }

    public void giveHands() {
        this.deck.undoCard();
        for (int i = 0; i < 2; i++ ){
            this.players.forEach(p -> {
                p.getHand().addCardToHand(this.deck.drawCard());
            });
        }
    }

    public void showCardsOnTable(){
        this.table.showCards();
    }

    public void showAllHands(){
        this.players.forEach(Player::showHand);
    }


    public void printResult(){
        this.players.forEach(p -> {
            p.getHand().addCardsToHand(this.table.getCardsOnTable());
            p.analyzeHand();
            p.printResult();
        });
    }

    public void addPlayer(Player player) {
        this.players.add(player);
    }

    public List<Player> getPlayers() {
        return players;
    }

    public Deck getDeck() {
        return deck;
    }

    public Table getTable() {
        return table;
    }
}
