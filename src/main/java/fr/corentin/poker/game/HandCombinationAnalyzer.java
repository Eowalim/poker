package fr.corentin.poker.game;

import fr.corentin.poker.cards.Card;
import fr.corentin.poker.cards.enums.CardSymbol;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class HandCombinationAnalyzer {
    private List<Card> cards;

    public static HandCombinationAnalyzer newAnalyzer() {
        return new HandCombinationAnalyzer();
    }

    public HandCombinationAnalyzer addHand(List<Card> cards) {
        this.cards = cards;
        return this;
    }

    public HandCombination analyze() {
        if (isQuinteFlushRoyale(this.cards)) {
            return HandCombination.QUINTE_FLUSH_ROYALE;
        } else if (isQuinteFlush(this.cards)) {
            return HandCombination.QUINTE_FLUSH;
        } else if (isFourOfKind(this.cards)) {
            return HandCombination.FOUR_OF_KIND;
        } else if (isFull(this.cards)) {
            return HandCombination.FULL;
        } else if (isFlush(this.cards)) {
            return HandCombination.FLUSH;
        } else if (isQuinte(this.cards)) {
            return HandCombination.QUINTE;
        } else if (isThreeOfKind(this.cards)) {
            return HandCombination.THREE_OF_KIND;
        } else if (isDoublePair(this.cards)) {
            return HandCombination.DOUBLE_PAIR;
        } else if (isPair(this.cards)) {
            return HandCombination.PAIR;
        }
        return HandCombination.HIGH_CARD;
    }

    private boolean isPair(List<Card> cards) {
        return getStream(cards)
                .filter(c -> c.getValue() == 2)
                .count() == 1;
    }

    private boolean isDoublePair(List<Card> cards) {
        return getStream(cards)
                .filter(c -> c.getValue() == 2)
                .count() >= 2;
    }

    private boolean isThreeOfKind(List<Card> cards) {
        return getStream(cards).anyMatch(c -> c.getValue() == 3);
    }

    private boolean isQuinte(List<Card> cards) {
        return containConsecutiveNumber(cards.subList(0, 5))
                || containConsecutiveNumber(cards.subList(1, 6))
                || containConsecutiveNumber(cards.subList(2, 7));
    }

    private boolean isFlush(List<Card> cards) {
        return hasAllSameColor(cards);
    }

    private boolean isFull(List<Card> cards) {
        return isPair(cards) && isThreeOfKind(cards);
    }

    private boolean isFourOfKind(List<Card> cards) {
        return getStream(cards)
                .filter(c -> c.getValue() == 4)
                .count() == 1;
    }

    private boolean isQuinteFlush(List<Card> cards) {
        return isQuinte(cards) && isFlush(cards);
    }

    private boolean isQuinteFlushRoyale(List<Card> cards) {
        Set<String> handValues = cards.stream().map(c -> c.getCardType().getFormattedCardValue()).collect(Collectors.toSet());

        return isQuinte(cards)
                && isFlush(cards)
                && handValues.containsAll(List.of(new String[]{"10", "J", "Q", "K", "A"}));
    }

    private boolean isHighCard(List<Card> cards) {
        return !isPair(cards)
                && !isDoublePair(cards)
                && !isThreeOfKind(cards)
                && !isQuinte(cards)
                && !isFlush(cards)
                && !isFull(cards)
                && !isFourOfKind(cards)
                && !isQuinteFlush(cards)
                && !isQuinteFlushRoyale(cards);
    }


    private Stream<Map.Entry<String, Long>> getStream(List<Card> cards) {
        return cards.stream()
                .collect(Collectors.groupingBy(c -> c.getCardType().getFormattedCardValue(), Collectors.counting()))
                .entrySet()
                .stream();
    }

    private boolean hasAllSameColor(List<Card> cards) {
        int nbDiamond = (int) cards.stream().filter(c -> c.getSymbol() == CardSymbol.DIAMOND).count();
        int nbHeart = (int) cards.stream().filter(c -> c.getSymbol() == CardSymbol.HEART).count();
        int nbSpades = (int) cards.stream().filter(c -> c.getSymbol() == CardSymbol.SPADES).count();
        int nbClub = (int) cards.stream().filter(c -> c.getSymbol() == CardSymbol.CLUB).count();

        return nbDiamond >= 5 || nbHeart >= 5 || nbSpades >= 5 || nbClub >= 5;
    }

    private boolean containConsecutiveNumber(List<Card> cards) {
        if (cards == null || cards.isEmpty()) return false;
        int last = -1;
        for (Card c : cards) {
            if (last != -1 && c.getOrder() != last + 1) {
                return false;
            }
            last = c.getOrder();
        }
        return true;
    }
}
