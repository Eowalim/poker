package fr.corentin.poker.player;

import fr.corentin.poker.cards.Card;
import fr.corentin.poker.game.HandCombination;
import fr.corentin.poker.game.HandCombinationAnalyzer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Hand {
    private final List<Card> cards;


    public Hand() {
        this.cards = new ArrayList<>();
    }

    public void sort() {
        Collections.sort(cards);
    }

    public void addCardsToHand(List<Card> cards) {
        this.cards.addAll(cards);
    }

    public void addCardToHand(Card card) {
        this.cards.add(card);
    }

    public void clear() {
        this.cards.clear();
    }

    public List<Card> getCards() {
        return cards;
    }

    public String getCardOfHand(){
        StringBuilder sb = new StringBuilder();
        this.cards.forEach(c -> {
            sb.append(c).append(" ");
        });
        return sb.toString();
    }
}
