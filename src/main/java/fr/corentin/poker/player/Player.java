package fr.corentin.poker.player;

import fr.corentin.poker.game.HandCombination;
import fr.corentin.poker.game.HandCombinationAnalyzer;

public class Player implements Comparable<Player> {

    private static int NB_PLAYER = 0;

    private final int id;

    private final Hand hand;

    private HandCombination combination;

    private int combinationPoints;

    public Player() {
        NB_PLAYER++;
        this.id = NB_PLAYER;
        this.hand = new Hand();
        this.combination = HandCombination.HIGH_CARD;
        this.combinationPoints = this.combination.getPoints();
    }

    public void analyzeHand() {
        if (this.hand.getCards().size() != 7) {
            throw new IllegalStateException("The hand of player " + this.id + " does not contain 7 cards");
        }

        //sort the cards in the hand
        this.hand.sort();

        //analyze the combination
        this.combination = HandCombinationAnalyzer.newAnalyzer().addHand(this.hand.getCards()).analyze();

        this.combinationPoints = this.combination.getPoints();
    }


    public void showHand(){
        System.out.println("Player #" + this.id + " => " + this.hand.getCardOfHand());
    }


    public void printResult(){
        System.out.println("Player #" + this.id + ": " + this.combination.toString() + " (" + getHand().getCardOfHand() + ")");
    }

    public Hand getHand() {
        return hand;
    }

    public int getId() {
        return id;
    }

    public HandCombination getCombination() {
        return combination;
    }

    public int getCombinationPoints() {
        return combinationPoints;
    }

    @Override
    public int compareTo(Player p) {
        return Integer.compare(getCombinationPoints(), p.getCombinationPoints());
    }
}
