package fr.corentin.poker.player;

import fr.corentin.poker.cards.Card;

import java.util.ArrayList;
import java.util.List;

public class Table {
    private final List<Card> cardsOnTable;

    public Table(){
        this.cardsOnTable = new ArrayList<>();
    }

    public void addCardOnTable(Card card){
        this.cardsOnTable.add(card);
    }

    public List<Card> getCardsOnTable() {
        return cardsOnTable;
    }

    public void showCards(){
        this.cardsOnTable.forEach(c ->{
            System.out.print(c + " ");
        });
        System.out.println();
    }
}
